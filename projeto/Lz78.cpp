/*
 * Arquivo de Implementação Lz78
 * 
 * Professor: Marcelo Caniato Renhe
 * 
 * Autor: André Luiz dos Reis - 201965004A
 * Data de Criação:  15/03/2021
*/

#include "./headers/Lz78.h"
#include <fstream>

//construtor
Lz78::Lz78(){
    //cout << "Criando Lz78" << endl;
}

//destrutor
Lz78::~Lz78(){
    //cout << "Destruindo Lz78" << endl;
}

//verifica se uma sequencia existe
bool Lz78::contemSequencia(string word){
    return sequencias.find(word) == sequencias.end()?false : true;
}

//realiza a compressão de uma string
void Lz78::inserir(string word){
    string aux = "";
    char c;
    for(int i=0; i< word.size(); i++){
        char c = word.at(i);
        if(contemSequencia(aux + c)){
            aux += c;
        }else{
            this->auxinserir(aux,c);
            aux = "";
        }
    }
    if(aux != "") this->auxinserir(aux,'\0'); 
    //SE O FINAL FOR UM SUBSEQUENCIA JAH EXISTENTE, COLOCA \0 COMO REPRESENTAÇÃO DO NULL
}

//atualiza o dicionario e o vetor com a string comprimida
void Lz78::auxinserir(string word, char c){
    sequencias.insert(std::pair<string,int>(word+c,sequencias.size()+1));
    int i = word == "" ? 0 : sequencias.find(word)->second;
    struct word teste(i,c);
    words.push_back(teste);
}

// imprime a compressão
void Lz78::imprimir(){
    for (auto it = words.begin(); it != words.end(); ++it)
        cout << '(' << it->index << "," << it->c << ")"; 
    cout << endl;
}

