/*
 * Arquivo base da Classe Lz78
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 * Data de Criação:  08/03/2021
*/

#ifndef LZ78_H
#define LZ78_H

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

using namespace std;

//estrutura para dicionário
struct word{
    char c;
    int index;

    word(int index, char c){
        this->index = index;
        this->c = c;
    };
};


class Lz78 {
    private:   
    unordered_map<string, int> sequencias; 
    vector<word> words; // output
    bool contemSequencia(string word);
    void auxinserir(string word, char c);

    public:
    Lz78();
    ~Lz78();     
    void inserir(string word);
    void imprimir();

};

#endif // LZ78_H
