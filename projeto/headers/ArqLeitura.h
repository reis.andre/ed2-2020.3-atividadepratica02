/*
 * Arquivo base da Classe ArqLeitura
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 * Data de Criação:  08/03/2021
*/

#ifndef ARQLEITURA_H
#define ARQLEITURA_H

#include <iostream>
#include <fstream>
#include <string>


using namespace std;


class ArqLeitura {
    private:   
    string nomeArq;
    ifstream* arq;
    
    public:
    ArqLeitura(string nomeArq);
    ~ArqLeitura(); 
    string lerLinha();

};

#endif // ARQLEITURA_H
