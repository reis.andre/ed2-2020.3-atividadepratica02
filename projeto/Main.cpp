/*
 * Arquivo principal do projeto
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Último dígito da matrícula entre 4 e 6: LZ78
 * 
 * Autor: André Luiz dos Reis - 201965004A
 * Data de Criação:  15/03/2021
*/

#include <iostream>
#include <iomanip>

#include "headers/ArqLeitura.h"
#include "headers/Lz78.h"

using namespace std;

int main(int argc, char *argv[])
{
    // Verifica se existe algum parâmetros
    if (argc != 2)
    {
        cout << "Eh necessario passar o arquivo de entrada como parametro" << endl;
        exit(1);
    }


   try{
    
        cout << "*** Iniciando o Programa ***" << endl << endl;        
        const char *entrada = argv[1]; 
        cout << "Arquivo de entrada: " << entrada << endl ;
        ArqLeitura h(entrada);
        Lz78 lz;
        lz.inserir(h.lerLinha());
        //cout << "foi" << endl;
        lz.imprimir();

   }catch(const std::invalid_argument& e){
       cout << "### ERROR ###" << endl;
       std::cerr << e.what() << '\n';

   }
   catch(const std::exception& e){
       cout << "### ERROR ###" << endl;
       std::cerr << e.what() << '\n';
   }

    cout << endl << "*** Encerando o Programa ***" << endl;
   
    return 0;
}