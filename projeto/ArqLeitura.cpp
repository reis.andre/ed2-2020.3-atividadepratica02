/*
 * Arquivo de Implementação ArqLeitura
 * 
 * Professor: Marcelo Caniato Renhe
 * 
 * Autor: André Luiz dos Reis - 201965004A
 * Data de Criação:  15/03/2021
*/

#include "./headers/ArqLeitura.h"

//cria o classe de leitura para um determinado arquivo, pode ler mais de uma linha, se necessário
ArqLeitura::ArqLeitura(string nomeArq){
    //cout << "Criando ArqLeitura" << endl;
    arq = new ifstream(nomeArq);
    if(!arq->is_open()) throw invalid_argument("Erro ao abrir arquivo!");
}

ArqLeitura::~ArqLeitura(){
    if(arq != NULL){
        arq->close();
        delete arq;
    }
    //cout << "Destruindo ArqLeitura" << endl;
}

//realiza a leitura de uma linha
string ArqLeitura::lerLinha(){

    if (arq->is_open()){
        string str;
        getline(*arq, str); //le a linha
        return str;
    }
    else{
        throw invalid_argument("Erro na Leitura da Linha!");
    } 
}