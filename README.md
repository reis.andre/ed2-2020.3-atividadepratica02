# ED2-2020.3-AtividadePratica02

Atividade Prática de Implementação
DCC012 - Estrutura de Dados II
2020.3 - ERE - UFJF
20/03/2020
**André Luiz dos Reis - 201965004AC**

Instruções Iniciais:

Estrutura de arquivos:
1) projeto: contém todos os arquivos .cpp e .h da aplicação;

2) scripts:

    2.1 - run.cdm: (Windows) compila o programa e inicia o executável 
    
    2.2 - run.sh: (Linux) compila o programa e inicia o executável 

Explicação do Método Main:

    * Ocorre a leitura da linha do arquivo para por parametro;

    * Ocorre a compreensão atráves do LZ78;

    * O resultado é printado na tela, não havendo opção para escrita em arquivo.
  
Caso houver dúvidas relacionadas à navegação ou execução do código, o responsável deverá ser consultado!